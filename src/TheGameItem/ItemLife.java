package TheGameItem;

import java.awt.Rectangle;

import javax.swing.ImageIcon;

import TheGame.Animation;
import TheGame.ItemType;
import TheGame.Items;

public class ItemLife extends Items
{
	
	//Number of update that this item is currently alive.
	private int timeAlive;
	
	//Total number of update loop this stays alive.
	private static final int MAX_TIME_ALIVE = 60 * 3;
	
	public ItemLife(int xPosition, int yPosition, int width, int height, String imageLocation, int duration)
	{
		
		itemType = ItemType.Life;
		itemBoundingBox = new Rectangle(xPosition, yPosition, width, height);
		glowAnimation = new Animation("Art/Items/itemGlow.png", 8, 64);
		
		dead = false;
		
		itemImage = new ImageIcon(imageLocation).getImage();
	}
	
	public ItemType getItemType()
	{
		return itemType;
	}
	
	public void update()
	{
		itemBoundingBox.y += ITEM_DROP;
		if(itemBoundingBox.y >= 600)
		{
			dead = true;
		}
	}
}
