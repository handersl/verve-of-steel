package TheGameEnemy;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.ArrayList;

import Audio.AudioPlayer;
import TheGame.Animation;
import TheGame.Player;
import TheGameProjectile.Projectile;

public abstract class Enemy 
{
	private static final int SCREEN_WIDTH = 600;
	private static final int SCREEN_HEIGHT = 600;
	
	//The bounding box for the enemy.
	protected Rectangle enemyBoundingBox;
	//The image for the enemy.
	protected Image enemyImage;
	//The amount of damage the enemy does on contact.
	protected int contactDamage;
	//The type of projectile that this enemy shoots.
	protected Projectile projectile;
	//The list of projectiles the enemy has.
	protected ArrayList<Projectile> listOfProjectiles;
	//Is the enemy dead or not.
	protected boolean dead;
	//The health
	protected int health;
	// movement variables
	protected int enemyMovementType;
	protected int enemyXVel;
	protected int enemyYVel;
	protected int enemyBoundingLeft;
	protected int enemyBoundingRight;
	protected int enemyBoundingTop;
	protected int enemyBoundingBottom;
	// reference to player, for targeting purposes
	protected Player player;
	//The animation for the explosion.
	protected Animation enemyExplosion;
	//Is the enemy exploded or not.
	protected boolean exploded;
	//Timer for when to leave the field
	protected int timer;
	
	protected Animation enemyBooster;
	
	public boolean hasJustDied = false;
	public boolean hasJustExploded = false;
	
	protected AudioPlayer died = new AudioPlayer("/Resources/Music/explosion2.mp3");
	
	protected int randomNumber = 1;
	
	//The update logic loop for the enemy.  To create a projectile just
	//add it to the list in this loop when the right time has come.
	public void update() 
	{

	}
	
	//Draw the enemy.
	public void paint(Graphics mainGraphic) 
	{
		Graphics2D enemyGraphic = (Graphics2D)mainGraphic;
		//Draw the enemy where the bounding box is.
		
		if(!dead)
		{
			enemyGraphic.drawImage(enemyImage, (int)(enemyBoundingBox.getX()), (int)(enemyBoundingBox.getY()), null);
			if(enemyYVel > 0) 
			{
				enemyGraphic.drawImage(
						enemyBooster.getImage(), 
						(int)(enemyBoundingBox.getX()), 
						(int)(enemyBoundingBox.getY()) - 25, 
						null);
			}
		}
		else if (!exploded)
		{
			enemyGraphic.drawImage(enemyExplosion.getImage(),
					(int)(enemyBoundingBox.getX()),
					(int)(enemyBoundingBox.getY()),
					null);
		}
		
		for(int i = 0; i < listOfProjectiles.size(); i++)
		{
			if(!listOfProjectiles.get(i).isDead())
			{
				listOfProjectiles.get(i).paint(mainGraphic);
			}
		}
	}
	
	//Return the bounding box.
	public Rectangle getBoundingBox()
	{
		return enemyBoundingBox;
	}
	
	//Get the projectiles made.
	public ArrayList<Projectile> getProjectiles()
	{
		return listOfProjectiles;
	}
	
	public void kill()
	{
		health = 0;
		dead = true;
	}
	
	//Apply the damage and check to see if the enemy is still alive.
	public void applyDamage(int damage)
	{
		health -= damage;
		if(health <= 0)
		{
			died.play();
			dead = true;
		}
	}
	
	//Is the enemy dead.
	public boolean isDead()
	{
		return dead;
	}
	
	public int getContactdamage()
	{
		return contactDamage;
	}
	
	public void doDeletions()
	{
		for(int i = listOfProjectiles.size() - 1; i >= 0; i--)
		{
			if(listOfProjectiles.get(i).isDead())
			{
				listOfProjectiles.remove(i);
			}
		}
	}
	
	public void setPlayer(Player targetPlayer)
	{
		player = targetPlayer;
	}
	
	//Is the enemy exploded.
	public boolean isExploded()
	{
		return exploded;
	}
	
	protected void setUpMovement()
	{
		if (enemyMovementType == 0)
		{
			if (enemyBoundingBox.x < SCREEN_WIDTH/3)
			{
				enemyBoundingLeft = SCREEN_WIDTH/16;
				enemyBoundingRight = SCREEN_WIDTH*7/16;
				enemyBoundingBottom = SCREEN_HEIGHT*1/4;
			}
			if (enemyBoundingBox.x > SCREEN_WIDTH*2/3)
			{
				enemyBoundingLeft = SCREEN_WIDTH*9/16;
				enemyBoundingRight = SCREEN_WIDTH*15/16;
				enemyBoundingBottom = SCREEN_HEIGHT*1/4;
			}
			if ((enemyBoundingBox.x >= SCREEN_WIDTH/3) && (enemyBoundingBox.x <= SCREEN_WIDTH*2/3))
			{
				enemyBoundingLeft = SCREEN_WIDTH/4;
				enemyBoundingRight = SCREEN_WIDTH*3/4;
				enemyBoundingBottom = SCREEN_HEIGHT*1/8;
			}
			
			enemyXVel = 0;
			enemyYVel = 4;
		}
		
		if (enemyMovementType == 1)
		{
			if ((enemyBoundingBox.x > 0) && (enemyBoundingBox.x + enemyBoundingBox.width < SCREEN_WIDTH))
			{
				if (enemyBoundingBox.y < 0)
				{
					enemyXVel = 0;
					enemyYVel = 4;
				}
			}
			else
			{
				if (enemyBoundingBox.y < SCREEN_HEIGHT/4)
				{
					enemyXVel = 2;
					enemyYVel = 2;
				}
				
				if (enemyBoundingBox.y > SCREEN_HEIGHT/2)
				{
					enemyXVel = 3;
					enemyYVel = -1;
				}
				
				
				if (enemyBoundingBox.y >= SCREEN_HEIGHT/4 && enemyBoundingBox.y <= SCREEN_HEIGHT/2)
				{
					enemyXVel = 3;
					enemyYVel = 1;
				}
			}
						
			if (enemyBoundingBox.x + enemyBoundingBox.width > SCREEN_WIDTH)
			{
				enemyXVel = -enemyXVel;
			}
		}
		
		if (enemyMovementType == 2)
		{
			if (enemyBoundingBox.y < 0)
			{
				enemyXVel = 3;
				enemyYVel = 2;
				
				enemyBoundingBottom = SCREEN_WIDTH*randomNumber/8;
			}
			
			else 
			{
				if (enemyBoundingBox.y < SCREEN_HEIGHT/2)
				{
					enemyXVel = 3;
					enemyYVel = -1;
					
					enemyBoundingBottom = SCREEN_WIDTH*2/8;
				}
				
				if (enemyBoundingBox.y >= SCREEN_HEIGHT/2)
				{
					enemyXVel = 3;
					enemyYVel = 1;
					
					enemyBoundingBottom = SCREEN_WIDTH*3/8;
				}
			}
		}
	}
	
	public void move()
	{
		// standard horizontal(?) back and forth
		if (enemyMovementType == 0)
		{
			if (timer >= 60 * 4)
			{
				enemyXVel = 0;
				enemyYVel = 4;
				
				enemyBoundingBox.y += enemyYVel;
			}
			else
			{
				if (enemyYVel > 0)
				{
					enemyBoundingBox.y += enemyYVel;
					
					if (enemyBoundingBox.y + enemyBoundingBox.height > enemyBoundingBottom)
					{
						enemyYVel = 0; // Note: will do the next if block too.
						enemyXVel = 3;
						
						enemyBoundingBox.y = enemyBoundingBottom - enemyBoundingBox.height;
					}
				}
				
				if (enemyYVel == 0)
				{
					enemyBoundingBox.x += enemyXVel;
				
					if (enemyBoundingBox.x < enemyBoundingLeft)
					{
						enemyBoundingBox.x = enemyBoundingLeft;
						enemyXVel = -enemyXVel;
					}
					if (enemyBoundingBox.x + enemyBoundingBox.width > enemyBoundingRight)
					{
						enemyBoundingBox.x = enemyBoundingRight - enemyBoundingBox.width;
						enemyXVel = -enemyXVel;
					}
				}
			}
		}
		
		// linear movement
		if (enemyMovementType == 1)
		{
			enemyBoundingBox.x += enemyXVel;
			enemyBoundingBox.y += enemyYVel;
		}
		
		// tries to line up with the player
		if (enemyMovementType == 2)
		{
			if (timer >= 60 * 4)
			{
				enemyXVel = 0;
				enemyYVel = 4;
				
				enemyBoundingBox.y += enemyYVel;
			}
			else
			{
				if (enemyYVel != 0)
				{
					if ((enemyBoundingBottom - enemyBoundingBox.y) > 0 && (enemyBoundingBottom - (enemyBoundingBox.y + enemyBoundingBox.height)) > 0)
					{
						enemyYVel = Math.abs(enemyYVel); // *(enemyBoundingBottom - enemyBoundingBox.y)/Math.abs(enemyBoundingBottom - enemyBoundingBox.y);						
					}
					/*
					else if ((enemyBoundingBottom - enemyBoundingBox.y) < 0 && (enemyBoundingBottom - (enemyBoundingBox.y + enemyBoundingBox.height)) < 0)
					{
						enemyYVel = -Math.abs(enemyYVel); // *(enemyBoundingBottom - (enemyBoundingBox.y + enemyBoundingBox.height))/Math.abs(enemyBoundingBottom - (enemyBoundingBox.y + enemyBoundingBox.height));						
					}
					*/
					else
					{
						enemyYVel = -Math.abs(enemyYVel);
					}
				
					enemyBoundingBox.y += enemyYVel;	
				
					if (Math.abs(enemyBoundingBottom - (enemyBoundingBox.y + enemyBoundingBox.height)) <= Math.abs(enemyYVel))
					{
						enemyBoundingBox.y = enemyBoundingBottom - enemyBoundingBox.height;
						enemyYVel = 0;
						enemyXVel = 4;	
					}
				}
				
				enemyXVel = (int) (Math.abs(enemyXVel)*
						(player.getPlayerCircle().getCenterX() - (enemyBoundingBox.x + enemyBoundingBox.width/2))/
						Math.abs(player.getPlayerCircle().getCenterX() - (enemyBoundingBox.x + enemyBoundingBox.width/2)));
				
				enemyBoundingBox.x += enemyXVel;
				
				if (Math.abs(player.getPlayerCircle().getCenterX() - (enemyBoundingBox.x + enemyBoundingBox.width/2)) <= Math.abs(enemyXVel))
				{
					enemyBoundingBox.x = (int) (player.getPlayerCircle().getCenterX() - enemyBoundingBox.width/2);
				}
				
				/*
				if (enemyBoundingBox.x + enemyBoundingBox.width < player.getPlayerCircle().x)
				{
					//enemyBoundingBox.y = enemyBoundingLeft;
					enemyXVel = Math.abs(enemyXVel);
				}
				else if (enemyBoundingBox.x > player.getPlayerCircle().x + player.getPlayerCircle().width)
				{
					//enemyBoundingBox.y = enemyBoundingRight - enemyBoundingBox.height;
					enemyXVel = -Math.abs(enemyXVel);
				}
				*/
			}
		}
		if (enemyMovementType == 3)
		{
			if (timer >= (60 * 5) - 0)
			{
				enemyXVel = 0;
				enemyYVel = 4;
				
				enemyBoundingBox.y += enemyYVel;
			}
			else if (timer >= 60)
			{
				enemyXVel = 0;
				enemyYVel = 0;
			}
			else
			{
				enemyBoundingBox.y += enemyYVel;
			}
		}
	}
	
	public int getHP()
	{
		return health;
	}
}
