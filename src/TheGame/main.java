package TheGame;

public class main 
{
	public static void main(String[] args) 
	{
		Engine engine = new Engine();
		
		while(engine.isRunning())
		{
			engine.update();
		}
	}

}
