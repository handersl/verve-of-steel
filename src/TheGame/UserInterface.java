package TheGame;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;

public class UserInterface {
	
	public static Font font;
	private int score;
	private int lives;
	private int scoreMult;
	private Image playerImage;
	private static final String PLAYER_MIDDLE_SPRITE_FILENAME = "Art/Planes/planeWhiteScore.png";

	public UserInterface(){
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		try {
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("Font/Minusio.ttf")));
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		font = new Font("Minusio", Font.BOLD, 50);
		
		score = 0;
		lives = 0;
		scoreMult = 0;
		playerImage = new ImageIcon(PLAYER_MIDDLE_SPRITE_FILENAME).getImage();
	}
	
	public void update(Player player, int score, int scoreM){
		//Need access to these variables
		lives = player.getLives();
		this.score = score;
		scoreMult = scoreM;
	}
	
	public void paint(Graphics mainGraphic){
		mainGraphic.setFont(font);
		mainGraphic.setColor(Color.BLACK);
		for (int x=0; x < lives; x++) 
		{
			mainGraphic.drawImage(playerImage, x*50, 550, null);
		}
		
		FontMetrics fm = mainGraphic.getFontMetrics();
		mainGraphic.drawString(Integer.toString(scoreMult) + "x " + Integer.toString(score), (int)(600 - fm.stringWidth("x " + Integer.toString(scoreMult) + " " + Integer.toString(score))), 595);
	}
	
	public void paintGameOver(Graphics mainGraphic)
	{
		mainGraphic.setFont(font);
		mainGraphic.setColor(getColorNumber((int)(System.currentTimeMillis())%11));
		FontMetrics fm = mainGraphic.getFontMetrics();
		mainGraphic.drawString("GAME OVER!!!", (int)(300 - fm.stringWidth("GAME OVER!!!")/2), 300 - fm.getHeight()/2 - fm.getHeight()/2);
		mainGraphic.drawString(Integer.toString(score) + " Pts", (int)(300 - fm.stringWidth(Integer.toString(score) + " Pts")/2), 300 - fm.getHeight()/4);
		mainGraphic.drawString("Press shoot to restart!", (int)(300 - fm.stringWidth("Press shoot to restart!")/2), 300 + fm.getHeight());
	}
	
	public void paintStartGame(Graphics mainGraphic)
	{
		mainGraphic.setFont(font);
		mainGraphic.setColor(Color.WHITE);
		FontMetrics fm = mainGraphic.getFontMetrics();
		mainGraphic.drawString("Verves of Steel", (int)(300 - fm.stringWidth("Verves of Steel")/2), 300 - fm.getHeight()/2 - fm.getHeight());
		mainGraphic.drawString("Press shoot to start!", (int)(300 - fm.stringWidth("Press shoot to start!")/2), 300 - fm.getHeight()/2 - fm.getHeight()/4);
	}
	
	private Color getColorNumber(int number)
	{
		switch(number){
		case 1:
			return Color.BLACK;
		case 2:
			return Color.RED;
		case 3:
			return Color.BLUE;
		case 4:
			return Color.GREEN;
		case 5:
			return Color.PINK;
		case 6:
			return Color.CYAN;
		case 7:
			return Color.DARK_GRAY;
		case 8:
			return Color.MAGENTA;
		case 9:
			return Color.YELLOW;
		case 10:
			return Color.ORANGE;
		}
		return Color.WHITE;
	}
}
