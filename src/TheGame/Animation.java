package TheGame;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Animation 
{
	//The whole animation
	private BufferedImage animation;
	//Current frame of the animation
	private BufferedImage currentImage;
	//Is the object finished animating
	private boolean done = false;
	//The width of one frame
	private int frameWidth;
	//Keeps track of the time passed through a counter
	private int timeCounter = -16;
	//Indicates the time in milliseconds for each frame
	private int frameRate;
	//The current frame number of the animation
	private int currentFrameNumber = -1;
	private int numberOfFrames;
	
	public Animation(String fileName, int numberOfFrames, int frameRate) 
	{
		try {
			animation = ImageIO.read(new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		frameWidth = animation.getWidth()/numberOfFrames;
		timeCounter = frameRate;
		this.frameRate = frameRate;
		this.numberOfFrames = numberOfFrames;
	}
	
	public Image getImage()
	{
		timeCounter += 16;
		
		if(timeCounter >= frameRate)
		{
			currentFrameNumber++;
			if(currentFrameNumber >= numberOfFrames) 
			{
				timeCounter = 0;
				currentFrameNumber = 0;
				currentImage = animation.getSubimage(0, 
						0, 
						frameWidth, 
						animation.getHeight());
				done = true;
			}
			else
			{
				currentImage = animation.getSubimage(
					currentFrameNumber * frameWidth,
					0,
					frameWidth,
					animation.getHeight());
			}
			timeCounter = 0;
		}
		return currentImage;
	}
	
	public boolean isDone()
	{
		return done;
	}
	
	public void reset()
	{
		timeCounter = -16;
		currentFrameNumber = -1;
		done = false;
	}
}
