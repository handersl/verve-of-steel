package TheGame;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;


//The engine class will take care of top level actions.
//It will control the update loop of the game and the
//rendering of the game.
public class Engine extends JFrame implements KeyListener
{
	//Don't know why this needs to be here.
	private static final long serialVersionUID = 1L;
	
	//If the engine is still running or not.
	private boolean running;
	
	private boolean doneDeleting;
	
	//The main level to update and render.
	Level currentLevel;
	
	//The time.  Used to check for the update time rate.
	private double timeElapsed;
	//The last time since the last time check.
	private double lastTimeUpdate;
	
	//The dimensions of the game.
	private static final int SCREEN_WIDTH = 600;
	private static final int SCREEN_HEIGHT = 600;
	
	//This is the rate at which the game will be run at.  Keeping
	//a constant framerate will make movement easier and better.
	private static final double UPDATE_RATE = 16.66666;
	
	public Engine()
	{
		running = true;
		
		//Create the level.
		currentLevel = new Level();
		//Load the level here.
		
		//Start the timer.
		timeElapsed = 0;
		lastTimeUpdate = System.currentTimeMillis();
		
		//Create the frame for the game.
		createFrame();
	}
	
	public void update()
	{
		timeElapsed += System.currentTimeMillis() - lastTimeUpdate;
		lastTimeUpdate = System.currentTimeMillis();
		
		if(timeElapsed >= UPDATE_RATE)
		{
			//Update the level.
			currentLevel.update();
			
			//Reset the time.
			timeElapsed = 0;
			
			//Repaint the level.
			repaint();
		}
	}
	
	//Creates the frame for the game based on the size set by the Engine.
	private void createFrame()
	{
		//Add the level here.
		add(currentLevel);
        setTitle("The Game");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        getContentPane().setPreferredSize(new Dimension(SCREEN_WIDTH - 10, SCREEN_HEIGHT - 10));
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(false);
        setFocusable(true);
        addNotify();
        requestFocus();
        addKeyListener(this);
	}

	//Is the engine still running.
	public boolean isRunning() 
	{
		return running;
	}
	
	public void keyReleased(KeyEvent key)
	{
		currentLevel.stopMove(key);
	}

	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void keyPressed(KeyEvent key) 
	{
		currentLevel.move(key);		
	}
}
