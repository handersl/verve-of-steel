package TheGameProjectile;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;

public class ProjectileArc extends Projectile
{
	private int time;
	private boolean magnitude;
	
	public ProjectileArc(int xPosition, int yPosition, int width, int height, Point2D.Double projectileSpeed, boolean isForPlayer, boolean projectileMagintude, String projectileString)
	{
		// import the following:
		// projectile's arc (how much it will curve)
		// projectile's speed
		
		projectileImage = new ImageIcon(projectileString).getImage();
		projectileBoundingBox = new Rectangle2D.Double(xPosition, yPosition, width, height);
		playerProjectile = isForPlayer;
		speed = projectileSpeed;
		time = 45;
		magnitude = projectileMagintude;
		
	}

	@Override
	public void update()
	{
		time += 1; // changing the amount time increases by changes the size of the look
		move();
	}
	
	private void move()
	{
		if (magnitude == true)
		{
		projectileBoundingBox = new Rectangle2D.Double(
				projectileBoundingBox.getX() + (Math.cos(30*Math.PI/time) * speed.getX() * 0.5), 
				projectileBoundingBox.getY() + (Math.sin(30*Math.PI/time) * speed.getY() * 0.5), 
				projectileBoundingBox.getWidth(), 
				projectileBoundingBox.getHeight());
		}
		else
		{
			projectileBoundingBox = new Rectangle2D.Double(
					projectileBoundingBox.getX() + (Math.sin(30*Math.PI/time) * speed.getX() * 0.5), 
					projectileBoundingBox.getY() + (Math.cos(30*Math.PI/time) * speed.getY() * 0.5), 
					projectileBoundingBox.getWidth(), 
					projectileBoundingBox.getHeight());
		}
		
	}	
}



