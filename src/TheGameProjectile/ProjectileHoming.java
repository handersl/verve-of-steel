package TheGameProjectile;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;

import TheGame.Player;

public class ProjectileHoming extends Projectile
{
	private Player player;
	
	public ProjectileHoming(int xPosition, int yPosition, int width, int height, double projectileSpeed, boolean isForPlayer, Player targetPlayer, String projectileString)
	{
		projectileImage = new ImageIcon(projectileString).getImage();
		projectileBoundingBox = new Rectangle2D.Double(xPosition, yPosition, width, height);
		playerProjectile = isForPlayer;
		speed = new Point2D.Double(projectileSpeed, projectileSpeed);
		player = targetPlayer;
	}
	
	@Override
	public void update()
	{
		move();
	}
	
	private void move()
	{
		projectileBoundingBox = new Rectangle2D.Double(
				projectileBoundingBox.getX() + (speed.x * normalizeX()), 
				projectileBoundingBox.getY() + (speed.y * normalizeY()), 
				projectileBoundingBox.getWidth(), 
				projectileBoundingBox.getHeight());
	}
	
	private double normalizeX()
	{
		return (player.getPlayerCircle().x - projectileBoundingBox.x)/getDistanceFromPlayer();
	}
	
	private double normalizeY()
	{
		return (player.getPlayerCircle().y - projectileBoundingBox.y)/getDistanceFromPlayer();
	}
	
	private double getDistanceFromPlayer()
	{
		return Math.sqrt(
				Math.pow((player.getPlayerCircle().x - projectileBoundingBox.x), 2) - 
				Math.pow((player.getPlayerCircle().y - projectileBoundingBox.y), 2));
	}
}