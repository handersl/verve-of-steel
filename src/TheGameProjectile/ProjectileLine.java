package TheGameProjectile;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import javax.swing.ImageIcon;

public class ProjectileLine extends Projectile
{
	private static final int DAMAGE = 1;
	
	//This class will make a projectile that moves in a straight line.
	public ProjectileLine(int xPosition, int yPosition, int width, int height, Point2D.Double projectileSpeed, boolean isForPlayer, String projectileFileName)
	{
		//Load the image and set the variables for the projectile.
		loadProjectile(xPosition, yPosition, width, height, new ImageIcon(projectileFileName).getImage(), isForPlayer, DAMAGE, projectileSpeed);
	}
	
	public ProjectileLine(int xPosition, int yPosition, Point2D.Double projectileSpeed, boolean isForPlayer, String projectileFileName)
	{
		loadProjectile(xPosition, yPosition, projectileFileName, isForPlayer, DAMAGE, projectileSpeed);
	}
	
	@Override
	public void update() 
	{
		projectileBoundingBox = new Rectangle2D.Double(projectileBoundingBox.getX() + speed.getX(), projectileBoundingBox.getY() + speed.getY(), projectileBoundingBox.getWidth(), projectileBoundingBox.getHeight());
	}
}
