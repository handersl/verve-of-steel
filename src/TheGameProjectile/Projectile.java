package TheGameProjectile;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;

import javax.swing.ImageIcon;

//The basic projectile class that all projectiles will come from.
public abstract class Projectile
{
	//The bounding box for the projectile.
	protected Rectangle2D.Double projectileBoundingBox;
	//The image for the projectile.
	protected Image projectileImage;
	//Is the projectile dead or not.
	protected boolean dead;
	//The damage this projectile does.
	protected int damage;
	//Who does this projectile belong to.  Ture is the player's projectile
	//and false is the enemies projectile.
	protected boolean playerProjectile;
	//The speed of the projectile.
	protected Point2D.Double speed;
	
	protected void loadProjectile(int x, int y, String projectileFileName, boolean isForPlayer, int dmg, Point2D.Double projectileSpeed)
	{
		//Load the image and set the variables for the projectile.
		ImageIcon imgIcon = new ImageIcon(projectileFileName);
		loadProjectile(x, y, imgIcon.getIconWidth(), imgIcon.getIconHeight(), imgIcon.getImage(), isForPlayer, dmg, projectileSpeed);
	}
	
	protected void loadProjectile(int x, int y, int width, int height, Image projectileImage, boolean isForPlayer, int dmg, Point2D.Double projectileSpeed)
	{
		this.projectileImage = projectileImage;
		this.projectileBoundingBox = new Rectangle2D.Double(x, y,width, height);
		playerProjectile = isForPlayer;
		speed = projectileSpeed;
		damage = dmg;
		dead = false;
	}
	
	//The update logic loop for the projectile.
	abstract public void update();
	
	//Draw the projectile.
	public void paint(Graphics mainGraphic) 
	{
		Graphics2D projectileGraphic = (Graphics2D)mainGraphic;
		//Draw the projectile where the bounding box is.
		if(!dead)
		{
			projectileGraphic.drawImage(projectileImage, (int)(projectileBoundingBox.getX()), (int)(projectileBoundingBox.getY()), null);
		}
	}
	
	//Return the bounding box.
	public Rectangle2D.Double getBoundingBox()
	{
		return projectileBoundingBox;
	}
	
	//Sets the projectile to dead.
	public void kill()
	{
		dead = true;
	}
	
	//Is the projectile dead.
	public boolean isDead()
	{
		return dead;
	}
	
	//Get the damage.
	public int getDamage()
	{
		return damage;
	}
	
	public boolean getProjectileType()
	{
		return playerProjectile;
	}
}
