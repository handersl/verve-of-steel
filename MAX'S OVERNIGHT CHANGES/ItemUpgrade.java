package TheGameItem;

import java.awt.Rectangle;

import javax.swing.ImageIcon;

import TheGame.Animation;
import TheGame.ItemType;
import TheGame.Items;

public class ItemUpgrade extends Items
{
	//Number of update that this item is currently alive.
	private int timeAlive;
	
	//Total number of update loop this stays alive.
	private static final int MAX_TIME_ALIVE = 60 * 3;
	
	public ItemUpgrade(int xPosition, int yPosition)
	{
		dead = false;
		itemImage = new ImageIcon("Art/Items/itemDamage.png").getImage();
		glowAnimation = new Animation("Art/Items/itemGlow.png", 8, 64);
		itemBoundingBox = new Rectangle(xPosition, yPosition, 50, 50);
		itemType = ItemType.Upgrade;
	}

	@Override
	public void update() 
	{
		timeAlive++;
		if(timeAlive >= MAX_TIME_ALIVE)
		{
			dead = true;
		}
	}
}
