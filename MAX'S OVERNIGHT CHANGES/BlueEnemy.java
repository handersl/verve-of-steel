package TheGameEnemy;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;

import TheGame.Animation;
import TheGame.Player;
import TheGameEnemy.Enemy;
import TheGameProjectile.Projectile;
import TheGameProjectile.ProjectileLine;

public class BlueEnemy extends Enemy
{
	// Projectile generation variables
	private int projectileTimer;
	private int projectileWave;
	private int projectileTickPerWave;
	private int projectileAttackTick;
	
	public BlueEnemy(int xPosition, int yPosition, int width, int height, String imageLocation, int movementType, Player player, String explodeAnimation)
	{
		enemyBooster = new Animation("Art/Boosters/boosterBlue.png", 8, 64);
		enemyExplosion = new Animation(explodeAnimation, 8, 64);
		
		listOfProjectiles = new ArrayList<Projectile>();
		
		enemyBoundingBox = new Rectangle(xPosition, yPosition, width, height);
		enemyImage = new ImageIcon(imageLocation).getImage();
		dead = false;
		health = 10; // TODO how much health?
		
		// projectile variables
		projectileTimer = 0;
		projectileWave = 0;
		projectileTickPerWave = 0;
		
		enemyMovementType = movementType;
		
		setUpMovement();
		
	}
	
	@Override
	public void update() 
	{
		if(!dead)
		{
			if(enemyXVel < 0)
			{
				enemyImage = new ImageIcon("Art/Planes/planeBlueLeft.png").getImage();
			}
			else if(enemyXVel > 0)
			{
				enemyImage = new ImageIcon("Art/Planes/planeBlueRight.png").getImage();
			}
			else
			{
				enemyImage = new ImageIcon("Art/Planes/planeBlue.png").getImage();
			}
			
			move();
			generateProjectiles();
		}
		
		if(enemyExplosion.isDone())
			exploded = true;
		
		for(Projectile projectile : listOfProjectiles)
		{
			projectile.update();
		}
	}
	
	// Handles the generation of a projectile pattern
	private void generateProjectiles()
	{
		if(!dead)
		{
			// Starts the pattern(?)
			
			projectileAttackTick += 1;
			
			if (projectileAttackTick >= 60)
			{
				projectileAttackTick = 0;
				
				projectileWave = 6;
				projectileTimer = 120; // 60 ticks per second
				projectileTickPerWave = projectileTimer/projectileWave;
			}
			
			// If it hasn't finished generating all the waves	
			if (projectileWave > 0)
			{
				// Generates the wave if the time has come
				if (projectileTimer <= projectileTickPerWave*projectileWave)
				{
					// Generates the wave. COnsists of a single downward ProjectileLine currently
					for (int i = 0; i < 1; i++) // can iterate through mulitple projectiles per wave
					{
						listOfProjectiles.add(new ProjectileLine(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, new Point2D.Double(0.0,4.0), false, "Art/Bullets/bulletBlue.png"));
					}
					
					// finished one wave
					projectileWave -= 1;
				}
				
				// finished one tick
				projectileTimer -= 1;
			}
		}
	}
}
