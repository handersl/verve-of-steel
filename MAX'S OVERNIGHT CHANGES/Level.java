package TheGame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.ws.handler.MessageContext.Scope;

import TheGameEnemy.BlueEnemy;
import TheGameEnemy.Enemy;
import TheGameEnemy.EnemyRed;
import TheGameItem.ItemLife;
import TheGameItem.ItemUpgrade;
import TheGameProjectile.Projectile;

//The level class controls the game logic and the game drawing.
public class Level extends JPanel
{	
	private static final long serialVersionUID = 1L;
	
	private Player player;
	private ArrayList<Enemy> listOfEnemies;
	private ArrayList<Items> listOfItems;
	
	private static final String BACKGROUND_IMAGE_FILENAME = "Art/backgrounds/clouds.png";
	private BufferedImage backgroundImage;
	
	private static final int STARTING_LOCATION_X = 300;
	private static final int STARTING_LOCATION_Y = 500;
	
	private Rectangle scrollBackgroundRect;
	
	private int numberOfWavesCleared;
	
	private int upgradeSinceLastUpdate;
	
	private static final int SCREEN_WIDTH = 600;
	private static final int SCREEN_HEIGHT = 600;
	
	private static final int SCROLL_SPEED = 2;
	
	private float enemyScreenShakeTime = 0;
	private static final int ENEMY_SHAKE_TIME = 512;
	
	//TESTING
	private int numberOfEnemiesToSpawn;
	private BlueEnemy newEnemy;
	private Random rng;
	
	//Create the level.  Load the images, place the player and prepare
	//to spawn infinite enemies.
	public Level()
	{
		numberOfWavesCleared = 0;
		
		scrollBackgroundRect = new Rectangle(0, SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT);
		
		rng = new Random();
		
		try 
		{
			backgroundImage = ImageIO.read(new File(BACKGROUND_IMAGE_FILENAME));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		player = new Player(STARTING_LOCATION_X, STARTING_LOCATION_Y);

		listOfEnemies = new ArrayList<Enemy>();
		listOfItems = new ArrayList<Items>();
		
		//TESTING
		newEnemy = new BlueEnemy(0, 0, 50, 50, "Art/Planes/planeBlue.png", 0, player, "Art/Explosions/explosionBlue.png");;
		listOfEnemies.add(newEnemy);
		numberOfEnemiesToSpawn = 4;
	}
	
	//Update the game.  Update the player, the projectiles and the monsters.
	//Time elapsed is always 1/60.
	public void update()
	{
		if(player.isScreenShaking())
		{
			setLocation(rng.nextInt(32) - 16, rng.nextInt(32) - 16);
		}
		else if (enemyScreenShakeTime > 0)
		{
			enemyScreenShakeTime -= 16;
			setLocation(rng.nextInt(4) - 2, rng.nextInt(4) - 2);
		}
		else
			setLocation(0, 0);

		
		//Update the player.
		player.update();
		
		for(Items item : listOfItems)
		{
			item.update();
			//Check for collisions between the item and the player and call apply item.
			if(player.getPlayerCircle().intersects(item.getBoundingBox()) && !item.isDead() && !player.isRespawning())
			{
				applyItem(item);
				item.kill();
			}
		}
		
		for(int i = listOfItems.size(); i < listOfItems.size() - 1; i--)
		{
			if(listOfItems.get(i).isDead())
			{
				listOfItems.remove(i);
			}
		}
		
		for(Enemy enemy : listOfEnemies)
		{
			//Update the enemy.
			enemy.update();
			
			if(enemy.isDead() && !enemy.hasJustDied) {
				enemy.hasJustDied = true;
				enemyScreenShakeTime = ENEMY_SHAKE_TIME;
			}
			
			//Check for collisions between the player and the enemy and the projectiles.
			if(!enemy.isDead() && player.getPlayerCircle().intersects(enemy.getBoundingBox()) && !player.isRespawning())
			{
				player.kill();
			}
			
			for(Projectile projectile : enemy.getProjectiles())
			{
				if(player.getPlayerCircle().intersects(projectile.getBoundingBox()) && !projectile.isDead() && !player.isRespawning())
				{
					player.kill();
					projectile.kill();
				}
				
				if(!new Rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT).contains(projectile.getBoundingBox()))
				{
					projectile.kill();
				}
			}
		}
		
		for(Projectile projectile : player.getProjectiles())
		{
			for(Enemy enemy : listOfEnemies)
			{
				if(enemy.getBoundingBox().intersects(projectile.getBoundingBox()) && !player.isRespawning() && !enemy.isDead())
				{
					enemy.applyDamage(projectile.getDamage());
					projectile.kill();
				}
				
				if(!new Rectangle(0,0,SCREEN_WIDTH, SCREEN_WIDTH).contains(projectile.getBoundingBox()))
				{
					projectile.kill();
				}
			}
		}
		
		//Delete the enemies here.
		for(int i = listOfEnemies.size() - 1; i >= 0; i--)
		{
			if(listOfEnemies.get(i).isExploded() && listOfEnemies.get(i).getProjectiles().size() == 0)
			{
				listOfEnemies.remove(i);
			}
		}
		
		spawnEnemiesAndItems();
		
		doDeletions();
	}
	
	private void spawnEnemiesAndItems()
	{
		int numberOfLiveEnemy = 0;
		for(Enemy enemy : listOfEnemies)
		{
			if(!enemy.isDead())
			{
				numberOfLiveEnemy++;
			}
			
			upgradeSinceLastUpdate++;
			
			// THIS IS A BLATANT PROBLEM: WE ARE POSSIBLY CREATING EXTRA ITEMS
			// WHEN THE ENEMY HAS EXPLODED BUT THE ENEMY'S BULLETS HAVEN'T COMPLETELY
			// LEFT THE SCREEN. WHAT THE FUCK. God save us and god save this code
			if(enemy.isExploded() && !enemy.hasJustExploded)
			{
				enemy.hasJustExploded = true;
				
				if(rng.nextInt(100) >=  80 && upgradeSinceLastUpdate >= 300)
				{
					ItemLife newLife = new ItemLife((int)(enemy.getBoundingBox().getX()), (int)(enemy.getBoundingBox().getY()), 50, 50, "Art/Items/itemLife.png", 10);
					listOfItems.add(newLife);
				}
				else if(rng.nextInt(100) <= 20 && upgradeSinceLastUpdate >= 300)
				{
					ItemUpgrade newUpgrade = new ItemUpgrade((int)(enemy.getBoundingBox().getX()), (int)(enemy.getBoundingBox().getY()));
					listOfItems.add(newUpgrade);
				}
				
				upgradeSinceLastUpdate = 0;
			}
		}
		
		if(numberOfLiveEnemy <= 0)
		{
			for(int i = 0; i < numberOfWavesCleared; i++)
			{	
				newEnemy = new BlueEnemy(rng.nextInt(SCREEN_WIDTH - 50), rng.nextInt(SCREEN_HEIGHT/4), 50, 50, "Art/Planes/planeBlue.png", 0 , player, "Art/Explosions/explosionBlue.png");
				newEnemy.setPlayer(player);
				listOfEnemies.add(newEnemy);
			}
			
			for(int i = 0; i < numberOfWavesCleared/2; i++)
			{
				EnemyRed newRedEnemy = new EnemyRed(rng.nextInt(SCREEN_WIDTH - 50), rng.nextInt(SCREEN_HEIGHT/4), 50, 50, "Art/Planes/planeRed.png", 0, player,"Art/Explosions/explosionRed.png");
				listOfEnemies.add(newRedEnemy);
			}
			
			numberOfWavesCleared++;
		}
	}
	
	//This method takes in an item to apply to the game depending on the item type.
	private void applyItem(Items item)
	{
		if(item.getItemType() == ItemType.Life)
		{
			player.giveLives();
		}
		else if(item.getItemType() == ItemType.Upgrade)
		{
			player.upgrade();
		}
	}
	
	//Draw everything.  Call all objects paint method.  This is
	//in turn called by Engines paint and repaint methods.
	public void paint(Graphics mainGraphic)
	{
		Graphics2D levelGraphics = (Graphics2D)mainGraphic;
		levelGraphics.drawImage(backgroundImage.getSubimage((int)scrollBackgroundRect.getX(), (int)scrollBackgroundRect.getY(), (int)scrollBackgroundRect.getWidth(), (int)scrollBackgroundRect.getWidth()), 0, 0, null);
		scrollBackgroundRect.setLocation(0, (int)(scrollBackgroundRect.getY() - SCROLL_SPEED));
		
		if(scrollBackgroundRect.getY() <= 0)
		{
			scrollBackgroundRect = new Rectangle(0, SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT);
		}
		
		player.paint(mainGraphic);
		
		for(int i = 0; i < listOfItems.size(); i++)
		{
			listOfItems.get(i).paint(mainGraphic);
		}
		
		for(int i = 0; i < listOfEnemies.size(); i++)
		{
			listOfEnemies.get(i).paint(mainGraphic);
		}
	}

	public void move(KeyEvent key) 
	{
		player.move(key);
	}
	
	public void stopMove(KeyEvent key)
	{
		player.stopMove(key);
	}
	
	public boolean doDeletions()
	{
		player.doDeletions();
		
		for(Enemy enemy : listOfEnemies)
		{
			enemy.doDeletions();
		}
		return true;
	}
}
