package TheGame;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;

public abstract class Items 
{
	//The type of item it is.
	protected ItemType itemType;
	//The bounding box of the item.
	protected Rectangle itemBoundingBox;
	//The image of the item.
	protected Image itemImage;
	
	protected Animation glowAnimation;
	//Is the item dead.
	protected boolean dead;
	
	//Update the item if it is not static.
	abstract public void update();
	
	//If the level detects that the item left the screen or
	//is collected this will kill it.
	public void kill()
	{
		dead = true;
	}
	
	//Check if the item is dead.
	public boolean isDead()
	{
		return dead;
	}
	
	//Get the collision box.
	public Rectangle getBoundingBox()
	{
		return itemBoundingBox;
	}
	
	public ItemType getItemType()
	{
		return itemType;
	}
	
	//Draw the item.
	public void paint(Graphics mainGraphic) 
	{
		if(!dead)
		{
			//System.out.println("item paint() is running...");
			Graphics2D itemGraphic = (Graphics2D)mainGraphic;
			itemGraphic.drawImage(glowAnimation.getImage(),
					(int) (itemBoundingBox.getX()) - 25,
					(int) (itemBoundingBox.getY()) - 25,
					null);
			itemGraphic.drawImage(itemImage, 
					(int) (itemBoundingBox.getX()),
					(int) (itemBoundingBox.getY()),
					null);
		}
	}
}
