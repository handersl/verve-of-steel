package TheGame;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import Audio.AudioPlayer;
import TheGameProjectile.Projectile;
import TheGameProjectile.ProjectileLine;

//The player class of the game.
public class Player
{
	//The bounding box for the player.
	private Rectangle playerSpriteBox;
	private Ellipse2D.Double playerCircle;
	private Image playerImage;
	private Animation playerExplosion;
	private Animation playerBooster;
	private ArrayList<Projectile> playerProjectiles;
	private ArrayList<Projectile> projectilesToAdd;
	private boolean dead;
	private boolean canSpawn;
	private Point velocity;
	private Point previousPosition;
	private int upgradeLevel;
	private float shotDirection;
	private float respawnTimer;
	private boolean respawning;
	private int lives;
	private double boost;
	private int flashCounter;
	
	private static final int PLAYER_SPEED = 5;
	private static final int  PLAYER_WIDTH = 50;
	private static final int PLAYER_HEIGHT = 50;
	private static final int HURTBOX_WIDTH = 15;
	private static final int HURTBOX_HEIGHT = 15;
	private static final int PROJECTILE_WIDTH = 20;
	private static final int PROJECTILE_HEIGHT = 20;
	private static final int PROJECTILE_SPEED = 15;
	private static final int RESPAWN_TIME = 1;
	private static final String PLAYER_MIDDLE_SPRITE_FILENAME = "Art/Planes/planeWhite.png";
	private static final String PLAYER_LEFT_SPRITE_FILENAME = "Art/Planes/planeWhiteLeft.png";
	private static final String PLAYER_RIGHT_SPRITE_FILENAME = "Art/Planes/planeWhiteRight.png";
	
	private static final String PLAYER_MIDDLE_FADE50_SPRITE_FILENAME = "Art/Planes/planeWhiteFade50.png";
	private static final String PLAYER_LEFT_FADE50_SPRITE_FILENAME = "Art/Planes/planeWhiteFade50Left.png";
	private static final String PLAYER_RIGHT_FADE50_SPRITE_FILENAME = "Art/Planes/planeWhiteFade50Right.png";
	private static final String PLAYER_MIDDLE_FADE75_SPRITE_FILENAME = "Art/Planes/planeWhiteFade75.png";
	private static final String PLAYER_LEFT_FADE75_SPRITE_FILENAME = "Art/Planes/planeWhiteFade75Left.png";
	private static final String PLAYER_RIGHT_FADE75_SPRITE_FILENAME = "Art/Planes/planeWhiteFade75Right.png";
	
	private static final String PLAYER_PROJECTILE_SPRITE_FILENAME = "Art/Bullets/bulletWhite.png";
	private static final String PLAYER_EXPLOSION_SPRITE_FILENAME = "Art/Explosions/explosionWhite.png";
	private static final String PLAYER_BOOSTER_SPRITE_FILENAME = "Art/Boosters/boosterWhite.png";
	
	private static int STARTING_LOCATION_X;
	private static int STARTING_LOCATION_Y;
	
	private boolean exploding = false;
	
	//Create the player.
	public Player(int xPos, int yPos)
	{
		STARTING_LOCATION_X = xPos;
		STARTING_LOCATION_Y = yPos;
		canSpawn = true;
		lives = 9999;
		respawning = false;
		upgradeLevel = 0;
		shotDirection = 0;
		velocity = new Point(0,0);
		playerSpriteBox = new Rectangle(xPos, yPos, PLAYER_WIDTH, PLAYER_HEIGHT);
		playerCircle = new Ellipse2D.Double(xPos + (PLAYER_WIDTH/2 - HURTBOX_WIDTH/2), yPos + (PLAYER_HEIGHT/2 - HURTBOX_HEIGHT/2), HURTBOX_WIDTH, HURTBOX_HEIGHT);
		playerProjectiles = new ArrayList<Projectile>();
		projectilesToAdd = new ArrayList<Projectile>();
		playerImage = new ImageIcon(PLAYER_MIDDLE_SPRITE_FILENAME).getImage();
		playerExplosion = new Animation(PLAYER_EXPLOSION_SPRITE_FILENAME, 8, 64);
		playerBooster = new Animation(PLAYER_BOOSTER_SPRITE_FILENAME, 8, 64);
		dead = false;
		boost = 1;
		flashCounter++;
	}
	
	//Update player logic.
	public void update()
	{
		if(!dead) 
		{
			previousPosition = playerSpriteBox.getLocation();
			playerSpriteBox.setLocation((int)(playerSpriteBox.getX() + velocity.getX() * boost), (int)(playerSpriteBox.getY() + velocity.getY() * boost));
			
			if(playerSpriteBox.getX() <= 0 || playerSpriteBox.getX() >= 550 || playerSpriteBox.getY() < 0 || playerSpriteBox.getY() >= 550)
			{
				playerSpriteBox.setLocation(previousPosition);
			}
			
			playerCircle = new Ellipse2D.Double(playerSpriteBox.getX() + (PLAYER_WIDTH/2 - playerCircle.getWidth()/2), playerSpriteBox.getY() + (PLAYER_HEIGHT/2 - playerCircle.getHeight()/2), HURTBOX_WIDTH, HURTBOX_HEIGHT);
			
			if(respawning)
			{
				respawnTimer += 1.0f/60;
				if(respawnTimer >= RESPAWN_TIME)
				{
					respawning = false;
					respawnTimer = 0;
				}
			}
		}
		
		else // if(dead)
		{
			if(playerExplosion.isDone()) 
			{
				exploding = false;
				if(lives > 0)
				{
					respawn();
				}
			}
		}
		
		for(int i = 0; i < playerProjectiles.size(); i++)
		{
			playerProjectiles.get(i).update();
		}
		
		playerProjectiles.addAll(projectilesToAdd);
		projectilesToAdd.clear();
		
		
		if(respawning) {
			flashCounter += 16;
			if(++flashCounter <= 128) {
				if(velocity.getX() < 0)
				{
					playerImage = new ImageIcon(PLAYER_LEFT_FADE50_SPRITE_FILENAME).getImage();
				}
				else if(velocity.getX() == 0)
				{
					playerImage = new ImageIcon(PLAYER_MIDDLE_FADE50_SPRITE_FILENAME).getImage();
				}
				else
				{
					playerImage = new ImageIcon(PLAYER_RIGHT_FADE50_SPRITE_FILENAME).getImage();
				}
			}
			else
			{
				if(velocity.getX() < 0)
				{
					playerImage = new ImageIcon(PLAYER_LEFT_FADE75_SPRITE_FILENAME).getImage();
				}
				else if(velocity.getX() == 0)
				{
					playerImage = new ImageIcon(PLAYER_MIDDLE_FADE75_SPRITE_FILENAME).getImage();
				}
				else
				{
					playerImage = new ImageIcon(PLAYER_RIGHT_FADE75_SPRITE_FILENAME).getImage();
				}
			}
			
			if(flashCounter >= 240)
				flashCounter = 0;
		}
		
		else if(!dead)
		{
			if(velocity.getX() < 0)
			{
				playerImage = new ImageIcon(PLAYER_LEFT_SPRITE_FILENAME).getImage();
			}
			else if(velocity.getX() == 0)
			{
				playerImage = new ImageIcon(PLAYER_MIDDLE_SPRITE_FILENAME).getImage();
			}
			else
			{
				playerImage = new ImageIcon(PLAYER_RIGHT_SPRITE_FILENAME).getImage();
			}
		}
	}
	
	public void respawn()
	{
		upgradeLevel = 0;
		dead = false;
		respawning = true;
		playerSpriteBox.setLocation(STARTING_LOCATION_X, STARTING_LOCATION_Y);
		playerCircle = new Ellipse2D.Double(playerSpriteBox.getX() + (PLAYER_WIDTH/2 - playerCircle.getWidth()/2), playerSpriteBox.getY() + (PLAYER_HEIGHT/2 - playerCircle.getHeight()/2), HURTBOX_WIDTH, HURTBOX_HEIGHT);
		playerExplosion.reset();
	}
	
	//Draw the player at the bounding box.
	public void paint(Graphics mainGraphic)
	{
		Graphics2D playerGraphic = (Graphics2D)mainGraphic;
		
		//While in respawn do the flashing animation here. TODO
		if(respawning) 
		{	
			playerGraphic.drawImage(playerImage, (int)(playerSpriteBox.getX()), (int)(playerSpriteBox.getY()), null);
			if(velocity.getY() < 0) {
				playerGraphic.drawImage(
						playerBooster.getImage(),
						(int)(playerSpriteBox.getX()),
						(int)(playerSpriteBox.getY()+25),
						null);
			}
		}
		
		else if(!dead)
		{	
			playerGraphic.drawImage(playerImage, (int)(playerSpriteBox.getX()), (int)(playerSpriteBox.getY()), null);
			if(velocity.getY() < 0) {
				playerGraphic.drawImage(
						playerBooster.getImage(),
						(int)(playerSpriteBox.getX()),
						(int)(playerSpriteBox.getY()+25),
						null);
			}
		}
		else //if(dead) 
		{
			playerGraphic.drawImage(playerExplosion.getImage(), (int)(playerSpriteBox.getX()), (int)(playerSpriteBox.getY()), null);
		}
		for(int i = 0; i < playerProjectiles.size(); i++)
		{
			if(!playerProjectiles.get(i).isDead())
			{
				playerProjectiles.get(i).paint(mainGraphic);
			}
		}
	}
	
	public void kill()
	{
		dead = true;
		exploding = true;
		lives--;
	}
	
	public ArrayList<Projectile> getProjectiles()
	{
		return playerProjectiles;
	}
	
	public Ellipse2D.Double getPlayerCircle()
	{
		return playerCircle;
	}

	public void move(KeyEvent key) 
	{
		if(!dead)
		{
			//Movement keys.
			if(key.getKeyCode() == KeyEvent.VK_UP && playerSpriteBox.getY() > 0)
			{
				velocity = new Point((int)(velocity.getX()), -PLAYER_SPEED);
			}
			
			if(key.getKeyCode() == KeyEvent.VK_DOWN && playerSpriteBox.getY() < 550)
			{
				velocity = new Point((int)(velocity.getX()), PLAYER_SPEED);
			}
			
			if(key.getKeyCode() == KeyEvent.VK_LEFT && playerSpriteBox.getX() > 0)
			{
				velocity = new Point(-PLAYER_SPEED, (int)(velocity.getY()));
			}
			
			if(key.getKeyCode() == KeyEvent.VK_RIGHT && playerSpriteBox.getX() < 550)
			{
				velocity = new Point(PLAYER_SPEED, (int)(velocity.getY()));
			}
			
			if(toggleBoost(key))
			{
				boost = ((int)(key.getKeyChar()) - 48) * .20 + 1;
			}
			
			if(key.getKeyCode() == KeyEvent.VK_A)
			{
				shotDirection = -1.5f;
			}
			else if(key.getKeyCode() == KeyEvent.VK_F)
			{
				shotDirection = 1.5f;
			}
			else if(key.getKeyCode() == KeyEvent.VK_S)
			{
				shotDirection = -1;
			}
			else if(key.getKeyCode() == KeyEvent.VK_D)
			{
				shotDirection = 1;
			}
			
			//Shoot keys.  TODO later.
			if(key.getKeyCode() == KeyEvent.VK_W && canSpawn && !respawning)
			{
				canSpawn = false;
				
				//Find the degree to change the shot direction to.
				
				//The degree between the three directions of shooting.
				int shotDegree = 30;
				//The tilt of all three shots.
				int aimdegree = (int)(30 * shotDirection);
				
				double x = PROJECTILE_SPEED * Math.cos((Math.PI/180)*(90+aimdegree));
				double y = -PROJECTILE_SPEED * Math.sin((Math.PI/180)*(90+aimdegree));
				
				if(upgradeLevel <= 1)
				{
					ProjectileLine newProjectile = new ProjectileLine((int)(playerCircle.getCenterX() - (PROJECTILE_WIDTH/2)*(1 + upgradeLevel)), (int)(playerCircle.getCenterY() - PROJECTILE_HEIGHT/2), PROJECTILE_WIDTH, PROJECTILE_HEIGHT, new Point2D.Double(shotDirection*PROJECTILE_SPEED, -PROJECTILE_SPEED), true, PLAYER_PROJECTILE_SPRITE_FILENAME);
					projectilesToAdd.add(newProjectile);
					
					if(upgradeLevel == 1)
					{
						ProjectileLine newProjectileMiddle = new ProjectileLine((int)(playerCircle.getCenterX()), (int)(playerCircle.getCenterY() - PROJECTILE_HEIGHT/2), PROJECTILE_WIDTH, PROJECTILE_HEIGHT, new Point2D.Double(shotDirection*PROJECTILE_SPEED, -PROJECTILE_SPEED), true, PLAYER_PROJECTILE_SPRITE_FILENAME);
						projectilesToAdd.add(newProjectileMiddle);
					}
				}
				else if(upgradeLevel >= 2)
				{
					x= -PROJECTILE_SPEED * Math.cos((Math.PI/180)*(90 + shotDegree + aimdegree));
					y = -PROJECTILE_SPEED * Math.sin((Math.PI/180)*(90 + shotDegree + aimdegree));
					ProjectileLine newProjectileLeft = new ProjectileLine((int)(playerCircle.getCenterX() - PROJECTILE_WIDTH/2 - (PROJECTILE_WIDTH/2)*(upgradeLevel - 2)), (int)(playerCircle.getCenterY() - PROJECTILE_HEIGHT/2), PROJECTILE_WIDTH, PROJECTILE_HEIGHT, new Point2D.Double(x, y), true, PLAYER_PROJECTILE_SPRITE_FILENAME);
					projectilesToAdd.add(newProjectileLeft);
					x = -PROJECTILE_SPEED * Math.cos((Math.PI/180)*(90 + aimdegree));
					y = -PROJECTILE_SPEED * Math.sin((Math.PI/180)*(90 + aimdegree));
					ProjectileLine newProjectileMiddle = new ProjectileLine((int)(playerCircle.getCenterX() - PROJECTILE_WIDTH/2 - (PROJECTILE_WIDTH/2)*(upgradeLevel - 2)), (int)(playerCircle.getCenterY() - PROJECTILE_HEIGHT/2), PROJECTILE_WIDTH, PROJECTILE_HEIGHT, new Point2D.Double(x, y), true, PLAYER_PROJECTILE_SPRITE_FILENAME);
					projectilesToAdd.add(newProjectileMiddle);
					x = -PROJECTILE_SPEED * Math.cos((Math.PI/180)*(90 - shotDegree + aimdegree));
					y = -PROJECTILE_SPEED * Math.sin((Math.PI/180)*(90 - shotDegree + aimdegree));
					ProjectileLine newProjectileRight = new ProjectileLine((int)(playerCircle.getCenterX() + PROJECTILE_WIDTH/2 + (PROJECTILE_WIDTH)*(upgradeLevel - 2)), (int)(playerCircle.getCenterY() - PROJECTILE_HEIGHT/2), PROJECTILE_WIDTH, PROJECTILE_HEIGHT, new Point2D.Double(x, y), true, PLAYER_PROJECTILE_SPRITE_FILENAME);
					projectilesToAdd.add(newProjectileRight);
					
					if(upgradeLevel == 3)
					{
						x = -PROJECTILE_SPEED * Math.cos((Math.PI/180)*(90 + aimdegree));
						y = -PROJECTILE_SPEED * Math.sin((Math.PI/180)*(90 + aimdegree));
						ProjectileLine newProjectile = new ProjectileLine((int)(playerCircle.getCenterX() + PROJECTILE_WIDTH/2), (int)(playerCircle.getCenterY() - PROJECTILE_HEIGHT/2), PROJECTILE_WIDTH, PROJECTILE_HEIGHT, new Point2D.Double(x, y), true, PLAYER_PROJECTILE_SPRITE_FILENAME);
						projectilesToAdd.add(newProjectile);
					}
				}
			}
		}
	}
	
	public void stopMove(KeyEvent key)
	{
		if(key.getKeyCode() == KeyEvent.VK_DOWN || key.getKeyCode() == KeyEvent.VK_UP)
		{
			velocity = new Point((int)(velocity.getX()), 0);
		}
		
		if(key.getKeyCode() == KeyEvent.VK_LEFT || key.getKeyCode() == KeyEvent.VK_RIGHT)
		{
			velocity = new Point(0, (int)(velocity.getY()));
		}
		
		if(key.getKeyCode() == KeyEvent.VK_D || key.getKeyCode() == KeyEvent.VK_A || key.getKeyCode() == KeyEvent.VK_S || key.getKeyCode() == KeyEvent.VK_F)
		{
			shotDirection = 0;
		}

		if(key.getKeyCode() == KeyEvent.VK_W)
		{
			if(!respawning)
				canSpawn = true;
		}
		
		if(toggleBoost(key))
		{
			boost = 1;
		}
	}
	
	private boolean toggleBoost(KeyEvent key)
	{
		return (key.getKeyCode() == KeyEvent.VK_1 || key.getKeyCode() == KeyEvent.VK_2 || key.getKeyCode() == KeyEvent.VK_3 || key.getKeyCode() == KeyEvent.VK_4 || key.getKeyCode() == KeyEvent.VK_5 || key.getKeyCode() == KeyEvent.VK_6 || key.getKeyCode() == KeyEvent.VK_7 ||
				key.getKeyCode() == KeyEvent.VK_8 || key.getKeyCode() == KeyEvent.VK_9);
	}
	
	public void upgrade()
	{
		upgradeLevel++;
		if(upgradeLevel > 3)
		{
			upgradeLevel = 3;
		}
	}
	
	public void giveLives()
	{
		lives++;
	}
	
	public int getLives()
	{
		return lives;
	}
	
	public boolean isRespawning()
	{
		return respawning;
	}
	
	public boolean isScreenShaking()
	{
		return exploding;
	}
	
	public void doDeletions()
	{
		for(int i = playerProjectiles.size() - 1; i >= 0; i--)
		{
			if(playerProjectiles.get(i).isDead())
			{
				playerProjectiles.remove(i);
			}
		}
	}
	
	public boolean isShaking()
	{
		return exploding;
	}
}
