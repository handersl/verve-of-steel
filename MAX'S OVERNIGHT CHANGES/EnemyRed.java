package TheGameEnemy;

import java.awt.Image;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;

import TheGame.Player;
import TheGameEnemy.Enemy;
import TheGameProjectile.Projectile;
import TheGameProjectile.ProjectileArc;
import TheGameProjectile.ProjectileLine;

public class EnemyRed extends Enemy
{
	// Projectile generation variables
	private int projectileTimer;
	private int projectileWave;
	private int projectileTickPerWave;
	private int projectileAttackTick;

	private static final String PROJECTILE_FILENAME = "Art/Bullets/bulletRed.png";
		
	public EnemyRed(int xPosition, 
			int yPosition, 
			int width, 
			int height, 
			String imageLocation, 
			int movementType, 
			Player player, 
			String explodeAnimation)
	{
		enemyExplosion = new TheGame.Animation(explodeAnimation, 8, 64);
		enemyBooster = new TheGame.Animation("Art/Boosters/boosterRed.png", 8, 64);
		
		listOfProjectiles = new ArrayList<Projectile>();
		
		enemyBoundingBox = new Rectangle(xPosition, yPosition, width, height);
		enemyImage = new ImageIcon("Art/Planes/planeRed.png").getImage();
		dead = false;
		health = 10; // TODO how much health?
		
		// projectile variables
		projectileTimer = 0;
		projectileWave = 0;
		projectileTickPerWave = 0;
		
		enemyMovementType = movementType;
		
		setUpMovement();
	}
	
	@Override
	public void update() 
	{
		if(!dead) {
			if(enemyXVel < 0)
			{
				enemyImage = new ImageIcon("Art/Planes/planeRedLeft.png").getImage();
			}
			else if(enemyXVel > 0)
			{
				enemyImage = new ImageIcon("Art/Planes/planeRedRight.png").getImage();
			}
			else
			{
				enemyImage = new ImageIcon("Art/Planes/planeRed.png").getImage();
			}
			
			move();
			generateProjectiles();
		}
		
		if(enemyExplosion.isDone())
			exploded = true;
		
		for(Projectile projectile : listOfProjectiles)
		{
			projectile.update();
		}
	}
	
	// Handles the generation of a projectile pattern
	private void generateProjectiles()
	{
		if(!dead)
		{
			// Starts the pattern(?)
			
			projectileAttackTick += 1;
			
			if (projectileAttackTick >= 60)
			{
				projectileAttackTick = 0;
				
				projectileWave = 6;
				projectileTimer = 120; // 60 ticks per second
				projectileTickPerWave = projectileTimer/projectileWave;
			}
			
			// If it hasn't finished generating all the waves	
			if (projectileWave > 0)
			{
				// Generates the wave if the time has come
				if (projectileTimer <= projectileTickPerWave*projectileWave)
				{
					// Generates the wave. COnsists of a single downward ProjectileLine currently
					
					// 		-- REALLY COOL SHAPES --
					listOfProjectiles.add(new ProjectileArc(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, 20, 20, new Point2D.Double( 0, -10), false, 0.5, PROJECTILE_FILENAME));
					listOfProjectiles.add(new ProjectileArc(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, 20, 20, new Point2D.Double( 7, -7), false, 0.5, PROJECTILE_FILENAME));
					listOfProjectiles.add(new ProjectileArc(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, 20, 20, new Point2D.Double( 10, 0), false, 0.5, PROJECTILE_FILENAME));
					listOfProjectiles.add(new ProjectileArc(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, 20, 20, new Point2D.Double( 7, 7), false, 0.5, PROJECTILE_FILENAME));
					listOfProjectiles.add(new ProjectileArc(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, 20, 20, new Point2D.Double( 0, 10), false, 0.5, PROJECTILE_FILENAME));
					listOfProjectiles.add(new ProjectileArc(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, 20, 20, new Point2D.Double( -7, 7), false, 0.5, PROJECTILE_FILENAME));
					listOfProjectiles.add(new ProjectileArc(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, 20, 20, new Point2D.Double( -10, 0), false, 0.5, PROJECTILE_FILENAME));
					listOfProjectiles.add(new ProjectileArc(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, 20, 20, new Point2D.Double( -7, -7), false, 0.5, PROJECTILE_FILENAME));
					/*
					listOfProjectiles.add(new ProjectileArc(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, enemyBoundingBox.width, enemyBoundingBox.height, new Point2D.Double( 10, -10), false, 0.5));
					listOfProjectiles.add(new ProjectileArc(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, enemyBoundingBox.width, enemyBoundingBox.height, new Point2D.Double( 5, -5), false, 0.5));
					listOfProjectiles.add(new ProjectileArc(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, enemyBoundingBox.width, enemyBoundingBox.height, new Point2D.Double( 10, 10), false, -0.5));
					listOfProjectiles.add(new ProjectileArc(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, enemyBoundingBox.width, enemyBoundingBox.height, new Point2D.Double( 5, 5), false, 0.5));
					listOfProjectiles.add(new ProjectileArc(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, enemyBoundingBox.width, enemyBoundingBox.height, new Point2D.Double( -10, 10), false, 0.5));
					listOfProjectiles.add(new ProjectileArc(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, enemyBoundingBox.width, enemyBoundingBox.height, new Point2D.Double( -5, 5), false, 0.5));
					listOfProjectiles.add(new ProjectileArc(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, enemyBoundingBox.width, enemyBoundingBox.height, new Point2D.Double( -10, -10), false, -0.5));
					listOfProjectiles.add(new ProjectileArc(enemyBoundingBox.x, enemyBoundingBox.y + enemyBoundingBox.height, enemyBoundingBox.width, enemyBoundingBox.height, new Point2D.Double( -5, -5), false, 0.5));
					*/
					
					// finished one wave
					projectileWave -= 1;
				}
				
				// finished one tick
				projectileTimer -= 1;
			}
		}
	}
}
